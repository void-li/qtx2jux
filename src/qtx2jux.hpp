/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QObject>
#include <QString>

#include "lvd/logger.hpp"

// ----------

namespace lvd::qtx2jux {

class Qtx2Jux : public QObject {
  Q_OBJECT public: LVD_LOGGER

 public:
  Qtx2Jux(QObject* parent = nullptr);
  ~Qtx2Jux() override;

 public slots:
  void execute(const QString& source,
               const QString& target);

 signals:
  void success(const QString& message = "");
  void failure(const QString& message = "");
};

}  // namespace lvd::qtx2jux
