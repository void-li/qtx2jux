/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <cstdlib>
#include <exception>

#include <QCommandLineParser>
#include <QFileInfo>
#include <QList>
#include <QMetaObject>
#include <QObject>
#include <QString>
#include <QStringList>

#include "lvd/application.hpp"
#include "lvd/logger.hpp"
#include "lvd/metatype.hpp"

#include "config.hpp"
#include "qtx2jux.hpp"

// ----------

int main(int argc, char* argv[]) {
  lvd::Metatype::metatype();

  lvd::Application::setApplicationName (lvd::qtx2jux::config::App_Name());
  lvd::Application::setApplicationVersion(lvd::qtx2jux::config::App_Vers());

  lvd::Application::setOrganizationName(lvd::qtx2jux::config::Org_Name());
  lvd::Application::setOrganizationDomain(lvd::qtx2jux::config::Org_Addr());

  lvd::Application app(argc, argv);

  lvd::Application::connect(&app, &lvd::Application::failure,
  [] (const QString& message) {
    if (!message.isEmpty()) {
      lvd::qStdErr() << message << Qt::endl;
    }

    lvd::Application::exit(1);
  });

  // ----------

  lvd::Logger::install();
  LVD_LOGFUN

  // ----------

  QCommandLineParser qcommandlineparser;
  qcommandlineparser.   addHelpOption();
  qcommandlineparser.addVersionOption();

  qcommandlineparser.addPositionalArgument("source",
                                           "Source file.",
                                           "<source>");

  qcommandlineparser.addPositionalArgument("target",
                                           "Target file.",
                                           "<target>");

  lvd::Logger::setup_arguments(qcommandlineparser);

  qcommandlineparser.process(app);

  lvd::Logger::parse_arguments(qcommandlineparser);
  lvd::Application::print();

  // ----------

  QStringList arguments = qcommandlineparser.positionalArguments();

  QString source;

  if (arguments.size() > 0) {
    source = arguments[0];
  }
  if (source.isEmpty()) {
    lvd::qStdErr() << QString("missing source file") << Qt::endl;
    qcommandlineparser.showHelp(1);
  }

  LVD_LOG_D() << "source:"
              << source;

  if (QFileInfo info(source); !info.isFile()) {
    lvd::qStdErr() << QString("no such source file '%1'").arg(source) << Qt::endl;
    return EXIT_FAILURE;
  }

  QString target;

  if (arguments.size() > 1) {
    target = arguments[1];
  }
  if (target.isEmpty()) {
    lvd::qStdErr() << QString("missing target file") << Qt::endl;
    qcommandlineparser.showHelp(1);
  }

  LVD_LOG_D() << "target:"
              << target;

  // ----------

  try {
    lvd::qtx2jux::Qtx2Jux qtx2jux;

    QObject::connect(&qtx2jux, &lvd::qtx2jux::Qtx2Jux::success,
    [] (const QString& message) {
      if (!message.isEmpty()) {
        lvd::qStdOut() << message << Qt::endl;
      }

      lvd::Application::exit(0);
    });

    QObject::connect(&qtx2jux, &lvd::qtx2jux::Qtx2Jux::failure,
    [] (const QString& message) {
      if (!message.isEmpty()) {
        lvd::qStdErr() << message << Qt::endl;
      }

      lvd::Application::exit(1);
    });

    QMetaObject::invokeMethod(&qtx2jux, [&] {
      qtx2jux.execute(source, target);
    }, Qt::QueuedConnection);

    return app.exec();
  }
  catch (const std::exception& ex) {
    LVD_LOG_C() << "exception:" << ex.what();
  }
  catch (...) {
    LVD_LOG_C() << "exception!";
  }

  return EXIT_FAILURE;
}
