find_package(Qt5 REQUIRED COMPONENTS Core)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

# ----------

if(EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/config.cpp")
  configure_file(
    "${CMAKE_CURRENT_SOURCE_DIR}/config.cpp"
    "${CMAKE_CURRENT_BINARY_DIR}/config.cpp" @ONLY
  )
else()
  configure_file(
    "${CMAKE_CURRENT_SOURCE_DIR}/config.cpp.in"
    "${CMAKE_CURRENT_BINARY_DIR}/config.cpp" @ONLY
  )
endif()

# ----------

add_library(qtx2jux_a STATIC
  config.cpp.in "${CMAKE_CURRENT_BINARY_DIR}/config.cpp"
  config.hpp
  qtx2jux.cpp
  qtx2jux.hpp
)

target_link_libraries(qtx2jux_a
  lvd-core
  Qt5::Core
)

# ----------

add_executable(qtx2jux
  main.cpp
)

target_link_libraries(qtx2jux
  qtx2jux_a
)

install(TARGETS qtx2jux
  RUNTIME DESTINATION "${CMAKE_INSTALL_FULL_BINDIR}"
)

# ----------

install(PROGRAMS qtx2jux.sh
          DESTINATION "${CMAKE_INSTALL_FULL_BINDIR}"
)
