/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "qtx2jux.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <chrono>
#include <cstring>
#include <functional>
#include <ratio>

#include <QByteArray>
#include <QFile>
#include <QIODevice>
#include <QList>
#include <QString>
#include <QStringList>
#include <QStringRef>
#include <QVector>
#include <QXmlStreamAttributes>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>

#include "lvd/shield.hpp"

// ----------

namespace {

class XFile : public QIODevice {
 public:
  XFile(const QString& name)
      : qfile_(name) {}

 public:
  bool allow_declaration() const { return allow_declaration_; }
  void set_allow_declaration(bool allow_declaration) {
    allow_declaration_ = allow_declaration;
  }

 public:
  bool open(OpenMode mode) override {
    if (!qfile_.open(mode)) {
      return false;
    }

    return QIODevice::open(mode);
  }

  void close()             override {
    qfile_.close();

    return QIODevice::close();
  }

  bool atEnd()       const override {
    if (!allow_declaration_) {
      return true;
    }

    return qfile_.atEnd();
  }

 public:
  qint64  readData(      char *data, qint64 maxSize) override {
    if (!allow_declaration_) {
      return 0;
    }

    if (buffer_.size() >= maxSize) {
               int ssize = static_cast<         int>(maxSize);
      unsigned int usize = static_cast<unsigned int>(maxSize);

      memcpy(data, buffer_.data(), usize);
      buffer_ =    buffer_.mid(ssize);

      return ssize;
    }

    {
      QByteArray line = qfile_.readLine();

      LVD_FINALLY {
        buffer_.append(line);
      };

      if (line.startsWith("<?xml ")) {
        if (found_declaration_ != true) {
          found_declaration_  = true;
        }
        else {
          allow_declaration_  = false;

          if (buffer_.size() >= 0x00000) {
                     int ssize = static_cast<         int>(buffer_.size());
            unsigned int usize = static_cast<unsigned int>(buffer_.size());

            memcpy(data, buffer_.data(), usize);
            buffer_.clear();

            return ssize;
          }
          else {
            return 0;
          }
        }
      }
    }

    if      (buffer_.size() >= maxSize) {
               int ssize = static_cast<         int>(maxSize);
      unsigned int usize = static_cast<unsigned int>(maxSize);

      memcpy(data, buffer_.data(), usize);
      buffer_ =    buffer_.mid(ssize);

      return ssize;
    }
    else if (buffer_.size() >= 0x00000) {
               int ssize = static_cast<         int>(buffer_.size());
      unsigned int usize = static_cast<unsigned int>(buffer_.size());

      memcpy(data, buffer_.data(), usize);
      buffer_.clear();

      return ssize;
    }
    else {
      return 0;
    }
  }

  qint64 writeData(const char *data, qint64 len    ) override {
    Q_UNUSED(data) return len;
  }

 private:
  QFile qfile_;

 private:
  bool  allow_declaration_ = true ;
  bool  found_declaration_ = false;

  QByteArray buffer_;
};

// ----------

struct Property {
  QString key;
  QString val;
};

struct Testcase {
  QString name;
  QString time;

  bool    failure = false;
  QString failure_message;

  bool    skipped = false;
  QString skipped_message;

  QString message_stdout;
  QString message_stderr;
};

struct Testsuite {
  QString name, name_space;
  QString time, time_stamp;

  quint32 failure = 0;
  quint32 skipped = 0;
  quint32 testsum = 0;

  QVector<Property > properties;
  QVector<Testcase > testcases ;

  void add_property(const Property& property) {
    properties.append(property);
  }

  void add_testcase(const Testcase& testcase) {
    testcases .append(testcase);

    failure += testcase .failure ? 1 : 0;
    skipped += testcase .skipped ? 1 : 0;
    testsum +=                     1    ;
  }
};

struct Testsuites {
  QString name;
  QString time;

  quint32 failure = 0;
  quint32 skipped = 0;
  quint32 testsum = 0;

  QVector<Testsuite> testsuites;

  void add_testsuite(const Testsuite& testsuite) {
    testsuites.append(testsuite);

    failure += testsuite.failure;
    skipped += testsuite.skipped;
    testsum += testsuite.testsum;
  }
};

}  // namespace

// ----------

namespace {

void parse_impl(QXmlStreamReader& reader,
                const std::function<bool(QXmlStreamReader&)>& parse_beg,
                const std::function<bool(QXmlStreamReader&)>& parse_end) {
  LVD_LOGFUN_LIKE(lvd::qtx2jux::Qtx2Jux)
  LVD_LOG_T() << reader.name();

  while (!reader.atEnd()) {
    auto token = reader.readNext();

    if      (token == QXmlStreamReader::StartElement) {
      LVD_LOG_D() << "parse beg"
                  << reader.name();

      if (parse_beg(reader)) {
        continue;
      }
    }
    else if (token == QXmlStreamReader::  EndElement) {
      LVD_LOG_D() << "parse end"
                  << reader.name();

      if (parse_end(reader)) {
        return;
      }
    }
  }
}

// ----------

void parse_TestCase_Environment_QtBuild     (QXmlStreamReader& reader,
                                             Testsuite&        testsuite) {
  LVD_LOGFUN_LIKE(lvd::qtx2jux::Qtx2Jux)
  LVD_LOG_T() << reader.name();

  Property property;
  property.key = "QtBuild";
  property.val = reader.readElementText();

  testsuite.add_property(property);
}

void parse_TestCase_Environment_QtVersion   (QXmlStreamReader& reader,
                                             Testsuite&        testsuite) {
  LVD_LOGFUN_LIKE(lvd::qtx2jux::Qtx2Jux)
  LVD_LOG_T() << reader.name();

  Property property;
  property.key = "QtVersion";
  property.val = reader.readElementText();

  testsuite.add_property(property);
}

void parse_TestCase_Environment_QTestVersion(QXmlStreamReader& reader,
                                             Testsuite&        testsuite) {
  LVD_LOGFUN_LIKE(lvd::qtx2jux::Qtx2Jux)
  LVD_LOG_T() << reader.name();

  Property property;
  property.key = "QTestVersion";
  property.val = reader.readElementText();

  testsuite.add_property(property);
}

void parse_TestCase_Environment(QXmlStreamReader& reader,
                                Testsuite&        testsuite) {
  LVD_LOGFUN_LIKE(lvd::qtx2jux::Qtx2Jux)
  LVD_LOG_T() << reader.name();

  parse_impl(reader,
  [&] (QXmlStreamReader& reader) {
    if      (reader.name() == "QtBuild") {
      parse_TestCase_Environment_QtBuild     (reader, testsuite);
      return true;
    }
    else if (reader.name() == "QtVersion") {
      parse_TestCase_Environment_QtVersion   (reader, testsuite);
      return true;
    }
    else if (reader.name() == "QTestVersion") {
      parse_TestCase_Environment_QTestVersion(reader, testsuite);
      return true;
    }

    return false;
  },
  [&] (QXmlStreamReader& reader) {
    if (reader.name() == "Environment") {
      return true;
    }

    return false;
  });
}

void parse_TestCase_TestFunction_Incident_Description(QXmlStreamReader& reader,
                                                      Testcase&         testcase,
                                                      const QString&    type,
                                                      const QString&    file,
                                                      const QString&    line) {
  LVD_LOGFUN_LIKE(lvd::qtx2jux::Qtx2Jux)
  LVD_LOG_T() << reader.name();

  if (type == "fail") {
    testcase.failure_message.append(file);
    testcase.failure_message.append(":"  );

    testcase.failure_message.append(line);
    testcase.failure_message.append(":\n");

    QString text = reader.readElementText();
    testcase.failure_message.append(text);
  }
}

void parse_TestCase_TestFunction_Incident_DataTag    (QXmlStreamReader& reader,
                                                      Testcase&         testcase) {
  LVD_LOGFUN_LIKE(lvd::qtx2jux::Qtx2Jux)
  LVD_LOG_T() << reader.name();

  if (!testcase.name.endsWith(")")) {
    testcase.name.append("(");
    testcase.name.append(reader.readElementText());
    testcase.name.append(")");
  }
}

void parse_TestCase_TestFunction_Incident(QXmlStreamReader& reader,
                                          Testcase&         testcase,
                                          const Testcase&   testcase_base,
                                          Testsuite&        testsuite,
                                          qint32&           testcount) {
  LVD_LOGFUN_LIKE(lvd::qtx2jux::Qtx2Jux)
  LVD_LOG_T() << reader.name();

  QXmlStreamAttributes attributes = reader.attributes();

  QString type = attributes.value("type").toString();
  QString file = attributes.value("file").toString();
  QString line = attributes.value("line").toString();

  LVD_FINALLY {
    if (type == "fail") {
      testcase.failure = true;

      testsuite.add_testcase(testcase);
      testcase = testcase_base;
      testcount ++;
    } else {
      testsuite.add_testcase(testcase);
      testcase = testcase_base;
      testcount ++;
    }
  };

  parse_impl(reader,
  [&] (QXmlStreamReader& reader) {
    if      (reader.name() == "Description") {
      parse_TestCase_TestFunction_Incident_Description(reader, testcase, type, file, line);
      return true;
    }
    else if (reader.name() == "DataTag") {
      parse_TestCase_TestFunction_Incident_DataTag    (reader, testcase);
      return true;
    }

    return false;
  },
  [&] (QXmlStreamReader& reader) {
    if (reader.name() == "Incident") {
      return true;
    }

    return false;
  });
}

void parse_TestCase_TestFunction_Message_Description(QXmlStreamReader& reader,
                                                     Testcase&         testcase,
                                                     const QString&    type,
                                                     const QString&    file,
                                                     const QString&    line) {
  LVD_LOGFUN_LIKE(lvd::qtx2jux::Qtx2Jux)
  LVD_LOG_T() << reader.name();

  QString text = reader.readElementText();

  if (type == "system") {
    const QString QSTDOUT = "lvd.test.test: __Q_STD_OUT__ ";
    if (text.startsWith(QSTDOUT)) {
      testcase.message_stdout.append(text.midRef(QSTDOUT.size()));
      return;
    }

    const QString QSTDERR = "lvd.test.test: __Q_STD_ERR__ ";
    if (text.startsWith(QSTDERR)) {
      testcase.message_stderr.append(text.midRef(QSTDERR.size()));
      return;
    }
  }

  if (type != "skip") {
    if (   !testcase.message_stderr.isEmpty()
        && !testcase.message_stderr.endsWith('\n')) {
      testcase.message_stderr.append("\n");
    }
  }

  if (type == "skip") {
    testcase.skipped_message.append(file);
    testcase.skipped_message.append(":"  );

    testcase.skipped_message.append(line);
    testcase.skipped_message.append(":\n");

    testcase.skipped_message.append(text);
  } else if (type == "qdebug") {
    testcase.message_stderr.append("[D] ");
  } else if (type == "qinfo" ) {
    testcase.message_stderr.append("[I] ");
  } else if (type == "qwarn" ) {
    testcase.message_stderr.append("[W] ");
  } else if (type == "system") {
    testcase.message_stderr.append("[C] ");
  } else {
    testcase.message_stderr.append("[ ] ");
  }

  if (type != "skip") {
    testcase.message_stderr.append(text);
  }
}

void parse_TestCase_TestFunction_Message_DataTag    (QXmlStreamReader& reader,
                                                     Testcase&         testcase) {
  LVD_LOGFUN_LIKE(lvd::qtx2jux::Qtx2Jux)
  LVD_LOG_T() << reader.name();

  if (!testcase.name.endsWith(")")) {
    testcase.name.append("(");
    testcase.name.append(reader.readElementText());
    testcase.name.append(")");
  }
}

void parse_TestCase_TestFunction_Message(QXmlStreamReader& reader,
                                         Testcase&         testcase,
                                         const Testcase&   testcase_base,
                                         Testsuite&        testsuite,
                                         qint32&           testcount) {
  LVD_LOGFUN_LIKE(lvd::qtx2jux::Qtx2Jux)
  LVD_LOG_T() << reader.name();

  QXmlStreamAttributes attributes = reader.attributes();

  QString type = attributes.value("type").toString();
  QString file = attributes.value("file").toString();
  QString line = attributes.value("line").toString();

  LVD_FINALLY {
    if (type == "skip") {
      testcase.skipped = true;

      testsuite.add_testcase(testcase);
      testcase = testcase_base;
      testcount ++;
    }
  };

  parse_impl(reader,
  [&] (QXmlStreamReader& reader) {
    if      (reader.name() == "Description") {
      parse_TestCase_TestFunction_Message_Description(reader, testcase, type, file, line);
      return true;
    }
    else if (reader.name() == "DataTag") {
      parse_TestCase_TestFunction_Message_DataTag    (reader, testcase);
      return true;
    }

    return false;
  },
  [&] (QXmlStreamReader& reader) {
    if (reader.name() == "Message") {
      return true;
    }

    return false;
  });
}

void parse_TestCase_TestFunction_Duration(QXmlStreamReader& reader,
                                          Testsuite&        testsuite,
                                          qint32&           testcount) {
  LVD_LOGFUN_LIKE(lvd::qtx2jux::Qtx2Jux)
  LVD_LOG_T() << reader.name();

  if (!testsuite.testcases.isEmpty() && testcount == 1) {
    bool msecs_ok;
    auto msecs = reader.attributes().value("msecs").toDouble(&msecs_ok);

    if (!msecs_ok) {
      return;
    }

    using  sec_t = std::chrono::duration<double            >;
    using msec_t = std::chrono::duration<double, std::micro>;

    msec_t msec =                           msec_t (msecs);
     sec_t  sec = std::chrono::duration_cast<sec_t>(msec );

    testsuite.testcases.last().time = QString::number(sec.count(), 'f', 12);
  }
}

void parse_TestCase_TestFunction(QXmlStreamReader& reader,
                                 Testsuite&        testsuite) {
  LVD_LOGFUN_LIKE(lvd::qtx2jux::Qtx2Jux)
  LVD_LOG_T() << reader.name();

  Testcase testcase;
  testcase.name = reader.attributes().value("name").toString();

  Testcase testcase_base = testcase;
  qint32   testcount = 0;

  parse_impl(reader,
  [&] (QXmlStreamReader& reader) {
    if      (reader.name() == "Incident") {
      parse_TestCase_TestFunction_Incident(reader, testcase, testcase_base, testsuite, testcount);
      return true;
    }
    else if (reader.name() == "Message") {
      parse_TestCase_TestFunction_Message (reader, testcase, testcase_base, testsuite, testcount);
      return true;
    }
    else if (reader.name() == "Duration") {
      parse_TestCase_TestFunction_Duration(reader,                          testsuite, testcount);
      return true;
    }

    return false;
  },
  [&] (QXmlStreamReader& reader) {
    if (reader.name() == "TestFunction") {
      return true;
    }

    return false;
  });
}

void parse_TestCase_Duration(QXmlStreamReader& reader,
                             Testsuite&        testsuite) {
  LVD_LOGFUN_LIKE(lvd::qtx2jux::Qtx2Jux)
  LVD_LOG_T() << reader.name();

  bool msecs_ok;
  auto msecs = reader.attributes().value("msecs").toDouble(&msecs_ok);

  if (!msecs_ok) {
    return;
  }

  using  sec_t = std::chrono::duration<double            >;
  using msec_t = std::chrono::duration<double, std::micro>;

  msec_t msec =                           msec_t (msecs);
   sec_t  sec = std::chrono::duration_cast<sec_t>(msec );

  testsuite.time = QString::number(sec.count(), 'f', 12);
}

void parse_TestCase(QXmlStreamReader& reader,
                    Testsuites&       testsuites) {
  LVD_LOGFUN_LIKE(lvd::qtx2jux::Qtx2Jux)
  LVD_LOG_T() << reader.name();

  QStringList data = reader.attributes().value("name").toString().split("::");

  QString name = data.last();
  QString path = data.mid(0, data.length() - 1).join(".");

  Testsuite testsuite;
  testsuite.name       = name;
  testsuite.name_space = path;

  LVD_FINALLY {
    testsuites.add_testsuite(testsuite);
  };

  parse_impl(reader,
  [&] (QXmlStreamReader& reader) {
    if      (reader.name() == "Duration") {
      parse_TestCase_Duration    (reader, testsuite);
      return true;
    }
    else if (reader.name() == "Environment") {
      parse_TestCase_Environment (reader, testsuite);
      return true;
    }
    else if (reader.name() == "TestFunction") {
      parse_TestCase_TestFunction(reader, testsuite);
      return true;
    }

    return false;
  },
  [&] (QXmlStreamReader& reader) {
    if (reader.name() == "TestCase") {
      return true;
    }

    return false;
  });
}

void parse(QXmlStreamReader& reader,
           Testsuites&       testsuites) {
  LVD_LOGFUN_LIKE(lvd::qtx2jux::Qtx2Jux)
  LVD_LOG_T() << reader.name();

  while (!reader.atEnd()) {
    auto token = reader.readNext();

    if (   token == QXmlStreamReader::StartDocument
        || token == QXmlStreamReader::StartElement) {
      if (reader.name() == "TestCase") {
        parse_TestCase(reader, testsuites);
        continue;
      }
    }
  }
}

}  // namespace

// ----------

namespace {

void write_Testsuites_Testsuite_Property(QXmlStreamWriter& writer,
                                         const Property&   property) {
  LVD_LOGFUN_LIKE(lvd::qtx2jux::Qtx2Jux)
  LVD_LOG_T();

  writer.writeEmptyElement("property");

  writer.writeAttribute("name" , property.key);
  writer.writeAttribute("value", property.val);
}

void write_Testsuites_Testsuite_Testcase(QXmlStreamWriter& writer,
                                         const Testcase&   testcase) {
  LVD_LOGFUN_LIKE(lvd::qtx2jux::Qtx2Jux)
  LVD_LOG_T();

  writer.writeStartElement("testcase");

  LVD_FINALLY {
    writer.writeEndElement();
  };

  if (!testcase.name.isEmpty()) {
    writer.writeAttribute("name", testcase.name);
  }
  if (!testcase.time.isEmpty()) {
    writer.writeAttribute("time", testcase.time);
  }

  if      (testcase.failure) {
    writer.writeEmptyElement("failure");

    if (!testcase.failure_message.isEmpty()) {
      writer.writeAttribute  ("message", testcase.failure_message.trimmed());
    }
  }
  else if (testcase.skipped) {
    if (!testcase.skipped_message.isEmpty()) {
      writer.writeTextElement("skipped", testcase.skipped_message.trimmed());
    } else {
      writer.writeEmptyElement("skipped");
    }
  }

  if (!testcase.message_stdout.isEmpty()) {
    writer.writeTextElement("system-out", testcase.message_stdout.trimmed());
  }

  if (!testcase.message_stderr.isEmpty()) {
    writer.writeTextElement("system-err", testcase.message_stderr.trimmed());
  }
}

void write_Testsuites_Testsuite(QXmlStreamWriter& writer,
                                const Testsuite&  testsuite) {
  LVD_LOGFUN_LIKE(lvd::qtx2jux::Qtx2Jux)
  LVD_LOG_T();

  writer.writeStartElement("testsuite");

  LVD_FINALLY {
    writer.writeEndElement();
  };

  if (!testsuite.name.isEmpty()) {
    writer.writeAttribute("name", testsuite.name);
  }
  if (!testsuite.time.isEmpty()) {
    writer.writeAttribute("time", testsuite.time);
  }

  if (!testsuite.name_space.isEmpty()) {
    writer.writeAttribute("package"  , testsuite.name_space);
  }

  if (!testsuite.time_stamp.isEmpty()) {
    writer.writeAttribute("timestamp", testsuite.time_stamp);
  }

  writer.writeAttribute("failures", QString::number(testsuite.failure));
  writer.writeAttribute("skipped" , QString::number(testsuite.skipped));
  writer.writeAttribute("tests"   , QString::number(testsuite.testsum));

  if (!testsuite.properties.isEmpty()) {
    writer.writeStartElement("properties");

    LVD_FINALLY {
      writer.writeEndElement();
    };

    for (const Property& property : testsuite.properties) {
      write_Testsuites_Testsuite_Property(writer, property);
    }
  }

  for (const Testcase& testcase : testsuite.testcases) {
    write_Testsuites_Testsuite_Testcase(writer, testcase);
  }
}

void write_Testsuites(QXmlStreamWriter& writer,
                      const Testsuites& testsuites) {
  LVD_LOGFUN_LIKE(lvd::qtx2jux::Qtx2Jux)
  LVD_LOG_T();

  writer.writeStartElement("testsuites");

  LVD_FINALLY {
    writer.writeEndElement();
  };

  if (!testsuites.name.isEmpty()) {
    writer.writeAttribute("name", testsuites.name);
  }
  if (!testsuites.time.isEmpty()) {
    writer.writeAttribute("time", testsuites.time);
  }

  writer.writeAttribute("failures", QString::number(testsuites.failure));
  writer.writeAttribute("skipped" , QString::number(testsuites.skipped));
  writer.writeAttribute("tests"   , QString::number(testsuites.testsum));

  for (const Testsuite& testsuite : testsuites.testsuites) {
    write_Testsuites_Testsuite(writer, testsuite);
  }
}

void write(QXmlStreamWriter& writer,
           const Testsuites& testsuites) {
  LVD_LOGFUN_LIKE(lvd::qtx2jux::Qtx2Jux)
  LVD_LOG_T();

  write_Testsuites(writer, testsuites);
}

}  // namespace

// ----------

namespace lvd::qtx2jux {

Qtx2Jux::Qtx2Jux(QObject* parent)
    : QObject(parent) {
  LVD_LOG_T();
}

Qtx2Jux::~Qtx2Jux() {
  LVD_LOG_T();
}

// ----------

void Qtx2Jux::execute(const QString& source,
                      const QString& target) {
  LVD_LOG_T();
  LVD_SHIELD;

  LVD_LOG_D() << "source:"
              << source;

  LVD_LOG_D() << "target:"
              << target;

  XFile xfile(source);
  xfile.open(QFile::ReadOnly);

  if (!xfile.isOpen()) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << "cannot open source file"
                        << source;

    emit failure(message);
    return;
  }

  Testsuites testsuites;

  while (true) {
    QXmlStreamReader reader(&xfile);
    LVD_LOG_D() << "reading document";

    parse(reader, testsuites);

    auto reader_error = reader.error();
    if (reader_error != QXmlStreamReader::                    NoError) {
      if (reader_error != QXmlStreamReader::PrematureEndOfDocumentError) {
        LVD_LOGBUF message;
        LVD_LOG_W(&message) << reader.errorString();

        emit failure(message);
        return;
      }

      break;
    }

    xfile.set_allow_declaration(true);
  }

  QFile qfile(target);
  qfile.open(QFile::WriteOnly);

  if (!qfile.isOpen()) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << "cannot open target file"
                        << target;

    emit failure(message);
    return;
  }

  if    (true) {
    QXmlStreamWriter writer(&qfile);
    writer.writeStartDocument();

    writer.setAutoFormatting(true);
    writer.setAutoFormattingIndent(2);

    write(writer, testsuites);
    writer.writeEndDocument();
  }

  emit success();

  LVD_SHIELD_FUN(&Qtx2Jux::failure);
}

}  // namespace lvd::qtx2jux
