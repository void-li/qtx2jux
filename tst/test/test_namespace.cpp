/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QObject>
#include <QString>

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

// ----------

namespace lvd::qtx2jux {

class TestNamespace : public Test {
  Q_OBJECT

 private slots:
  void test_namespace() {
    QVERIFY(true);
  }

  // ----------

 private slots:
  void init() {
    Test::init();
  }

  void initTestCase() {
    Test::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      Test::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      Test::cleanupTestCase();
    };
  }
};

}  // namespace lvd::qtx2jux

LVD_TEST_MAIN(lvd::qtx2jux::TestNamespace)
#include "lvd/test_namespace.moc"  // IWYU pragma: keep
