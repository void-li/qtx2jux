#!/bin/sh
# Copyright © 2021 Luca Lovisa <opensource@void.li>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://www.wtfpl.net/ for more details.
# SPDX-License-Identifier: WTFPL
#
set -e -x

# ----------

qtx2jux_path=$1
[ -n "$qtx2jux_path" ]

qtx2jux_test=$2
[ -n "$qtx2jux_test" ]

qtx2jux_data=$3
[ -n "$qtx2jux_data" ]

# ----------

eval "$qtx2jux_test" -o "$qtx2jux_data.tmp,xml" || true
eval "$qtx2jux_path"    "$qtx2jux_data.tmp" "$qtx2jux_data"
rm                      "$qtx2jux_data.tmp"

sed -Ei 's/value="(|)[0-9]+\.[0-9]+\.[0-9]+(||||)"/value="\1\1%{[0-9]+\\.[0-9]+\\.[0-9]+}"/'         "$qtx2jux_data"
sed -Ei 's/value="Qt [0-9]+\.[0-9]+\.[0-9]+ [^"]+"/value="Qt %{[0-9]+\\.[0-9]+\\.[0-9]+} %{[^"]*}"/' "$qtx2jux_data"

sed -Ei 's/time="([0-9]+\.[0-9]+)"/time="%{[0-9]+\\.[0-9]+}"/'                                       "$qtx2jux_data"

qtx2jux_curr=$(pwd | sed -e 's/[]\/$*.^[]/\\&/g')
sed  -i "s/$qtx2jux_curr/%{.+}/"                                                                     "$qtx2jux_data"
