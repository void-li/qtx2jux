/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QFile>
#include <QIODevice>
#include <QList>
#include <QMetaObject>
#include <QObject>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QRegularExpressionMatchIterator>
#include <QString>
#include <QStringList>
#include <QTemporaryFile>

#include "qtx2jux.hpp"
#include "test_config.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

using namespace lvd::qtx2jux;

// ----------

class Qtx2JuxTest : public Test {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void qtx2jux() {
    QFETCH(QString, test_name);

    QString sample_tst_path = Current_Binary_Dir() + "/test/" + test_name;
    QString sample_xml_path = Current_Source_Dir() + "/test/" + test_name + ".xml";

    QTemporaryFile qtx2jux_source;
    qtx2jux_source.open();
    qtx2jux_source.close();

    QTemporaryFile qtx2jux_target;
    qtx2jux_target.open();
    qtx2jux_target.close();

    int ret = execute(sample_tst_path, "-o", qtx2jux_source.fileName() + ",xml");
    QVERIFY(ret >= 0);

    Qtx2Jux qtx2jux;

    QSignalSpy spy(&qtx2jux, &Qtx2Jux::success);
    QVERIFY(spy.isValid());

    QMetaObject::invokeMethod(this, [&] {
      qtx2jux.execute(qtx2jux_source.fileName(),
                      qtx2jux_target.fileName());
    }, Qt::QueuedConnection);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    qtx2jux_target.open();
    QString target_data = qtx2jux_target.readAll();
    qtx2jux_target.close();

    QFile qfile(sample_xml_path);
    qfile. open(QFile::ReadOnly);

    QVERIFY(qfile.isOpen());

    QString sample_data = qfile.readAll();
    QString sample_copy = sample_data;

    qfile.close();

    QRegularExpression sample_exprs_RX("\\%{(.+?)}");
    QStringList        sample_exprs;

    auto   match_it = sample_exprs_RX.globalMatch(sample_copy);
    while (match_it.hasNext()) {
      auto match = match_it.next();

      QString sample_item = match.captured(0);
      QString sample_expr = match.captured(1);

      int index = sample_data.indexOf(sample_item);

      sample_data.replace(index, sample_item.size(), QString("%{%1}").arg(sample_exprs.size()));
      sample_exprs.append(sample_expr);
    }

    sample_data = "^" + QRegularExpression::escape(sample_data) + "$";

    for (int i = 0; i < sample_exprs.size(); i ++) {
      QString sample_expr = sample_exprs[i];
      QString sample_item = QString("\\%\\{%1\\}").arg(i);

      sample_data.replace(sample_item, sample_expr);
    }

    auto regex_master = QRegularExpression(sample_data);
    auto match_master = regex_master.match(target_data);

    QVERIFY(match_master.hasMatch());
  }

  void qtx2jux_data() {
    QTest::addColumn<QString>("test_name");

    QTest::addRow("test_success"             ) << "test_success";
    QTest::addRow("test_failure"             ) << "test_failure";
    QTest::addRow("test_skipped"             ) << "test_skipped";
    QTest::addRow("test_several"             ) << "test_several";
    QTest::addRow("test_message"             ) << "test_message";
    QTest::addRow("test_message_to_stdout"   ) << "test_message_to_stdout";
    QTest::addRow("test_message_to_stderr"   ) << "test_message_to_stderr";
    QTest::addRow("test_namespace"           ) << "test_namespace";
    QTest::addRow("test_multiple_test_trials") << "test_multiple_test_trials";
    QTest::addRow("test_multiple_test_suites") << "test_multiple_test_suites";
  }

  // ----------

 private slots:
  void init() {
    Test::init();
  }

  void initTestCase() {
    Test::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      Test::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      Test::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(Qtx2JuxTest)
#include "qtx2jux_test.moc"  // IWYU pragma: keep
