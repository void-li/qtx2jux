/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "settings.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <algorithm>

#include <QCoreApplication>
#include <QMutex>
#include <QMutexLocker>
#include <QStandardPaths>
#include <QStringList>

#include "config.hpp"

// ----------

namespace {

QString make_app_name() {
  QString app_name = QCoreApplication:: applicationName  ();
  app_name = app_name.          toLower();

  return app_name;
}

QString make_org_addr() {
  QString org_addr = QCoreApplication::organizationDomain();

  QStringList org_list = org_addr.split('.');
  std::reverse(org_list.begin(), org_list.end());

  org_addr = org_list.join('.').toLower();

  return org_addr;
}

QString make_cfg_path(const QString& delimiter, const QString& cfg_path) {
  QString app_name = make_app_name();
  QString org_addr = make_org_addr();

  return QString("%1%2%3%4%5.conf").arg(
    cfg_path, delimiter, org_addr, delimiter, app_name
  );
}

}  // namespace

// ----------

namespace {

class Voodoo {
 public:
  Voodoo(QObject* parent);
  ~Voodoo();

  operator QObject*() const {
    return parent_;
  }

 private:
  QObject*     parent_;
  QMutexLocker locker_;

  QString app_name_;
  QString org_name_;

 private:
  static QMutex& qmutex() {
    static QMutex  qmutex;
    return qmutex;
  }
};

Voodoo::Voodoo(QObject* parent)
    : parent_(parent),
      locker_(&qmutex()) {
  app_name_ = QCoreApplication:: applicationName();
  org_name_ = QCoreApplication::organizationName();

  QString app_name = make_app_name();
  QCoreApplication:: setApplicationName(app_name);

  QString org_name = make_org_addr();
  QCoreApplication::setOrganizationName(org_name);

  static
  bool initialized = false;
  if (!initialized) {
    QSettings::setPath(QSettings::   IniFormat, QSettings::SystemScope, lvd::config::SystemPath_Etc());
    QSettings::setPath(QSettings::NativeFormat, QSettings::SystemScope, lvd::config::SystemPath_Etc());

    initialized = true;
  }
}

Voodoo::~Voodoo() {
  QCoreApplication:: setApplicationName(app_name_);
  QCoreApplication::setOrganizationName(org_name_);
}

}  // namespace

// ----------

namespace lvd {

Settings::Settings(const QString& organization,
                   const QString& application,
                   QObject*       parent)
    : QSettings(organization,
                application,
                Voodoo(parent)) {}

Settings::Settings(Scope          scope,
                   const QString& organization,
                   const QString& application,
                   QObject*       parent)
    : QSettings(scope,
                organization,
                application,
                Voodoo(parent)) {}

Settings::Settings(Format         format,
                   Scope          scope,
                   const QString& organization,
                   const QString& application,
                   QObject*       parent)
    : QSettings(format,
                scope,
                organization,
                application,
                Voodoo(parent)) {}

Settings::Settings(const QString& fileName,
                   Format         format,
                   QObject*       parent)
    : QSettings(fileName,
                format,
                Voodoo(parent)) {}

#if QT_VERSION >= QT_VERSION_CHECK(5, 13, 0)
Settings::Settings(Scope          scope,
                   QObject*       parent)
    : QSettings(scope,
                Voodoo(parent)) {}
#endif

Settings::Settings(QObject*       parent)
    : QSettings(Voodoo(parent)) {}

// ----------

QString Settings::system_config() {
  return make_cfg_path("/", config::SystemPath_Etc());
}

QString Settings::client_config() {
  return make_cfg_path("/", QStandardPaths::writableLocation(QStandardPaths::ConfigLocation));
}

QString Settings::custom_config() {
  return make_cfg_path(".", "");
}
}  // namespace lvd
