/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QIODevice>
#include <QList>
#include <QMessageLogContext>
#include <QProcess>
#include <QStringRef>
#include <QVector>
#include <QtTest>

// ----------

namespace {

const int   Logfile_Chunks =  8192 ;
const char* Logfile_Suffix = ".tmp";

}  // namespace

// ----------

namespace {
using namespace lvd;

QtMessageHandler qtMessageHandler = nullptr;

void message_handler(QtMsgType                 type,
                     const QMessageLogContext& context,
                     const QString&            message) {
  Logger::Level level = static_cast<Logger::Level>(type);

  if (!Logger::LevelIsBuiltIn(level)) {
    type = QtMsgType::QtDebugMsg;
  }

  if (qtMessageHandler) {
    qtMessageHandler(type, context, message);
  }
}

}  // namespace

// ----------

namespace lvd::test {

class Test::IODevice : public QIODevice {
  using TStream = QTextStream*;

 public:
  IODevice(const QString& prefix,
           const TStream& stdtxt)
      : prefix_(prefix),
        stdtxt_(stdtxt) {}

 private:
  qint64 readData (      char* data, qint64 maxlen) override;

  qint64 writeData(const char* data, qint64    len) override;

 private:
  QString prefix_;
  TStream stdtxt_;
};

qint64 Test::IODevice::readData (      char *data, qint64 maxlen) {
  Q_UNUSED(maxlen) Q_UNUSED(data)
  return -1;
}

qint64 Test::IODevice::writeData(const char *data, qint64    len) {
  QByteArray message(data, static_cast<int>(len));

  if (logfiles_.isEmpty()) {
    (*stdtxt_) << message;
    (*stdtxt_).flush();

    return len;
  }

  LVD_LOG_C() >> &QDebug::noquote
              << prefix_
              << message;

  return len;
}

// ----------

Test::Test(QObject* parent)
    : QObject(parent) {
  for (QString& logfile : logfiles_) {
    QString  bakfile = logfile + Logfile_Suffix;

    bool success = true;

    QFile bfile(bakfile);
    if (bfile.exists()) {
      success &= bfile.remove();
    }

    QFile lfile(logfile);
    if (lfile.exists()) {
      success &= lfile.rename(bakfile);
    }

    if (!success) {
      Q_ASSERT(success);
    }
  }

  if (!logfiles_gathered_) {
    QStringList arguments = QCoreApplication::arguments();

    for (QString& argument : arguments) {
      if (   argument.endsWith(","   "xml")
          || argument.endsWith(",lightxml")
          || argument.endsWith(",xunitxml")) {
        QString logfile = argument.splitRef(',').first().toString();

        if (logfile != "-") {
          logfiles_.append(logfile);
        }
      }
    }

    logfiles_gathered_ = true;
  }

  try {
    stdout_device_ = new IODevice("__Q_STD_OUT__", &qStdOut());
    stdout_device_->open(IODevice::WriteOnly);

    stdout_stream_ = new QTextStream(stdout_device_);

    Q_ASSERT(lvd::__impl__::qStdOutPtr == nullptr);
    lvd::__impl__::qStdOutPtr = stdout_stream_;

    stderr_device_ = new IODevice("__Q_STD_ERR__", &qStdErr());
    stderr_device_->open(IODevice::WriteOnly);

    stderr_stream_ = new QTextStream(stderr_device_);

    Q_ASSERT(lvd::__impl__::qStdErrPtr == nullptr);
    lvd::__impl__::qStdErrPtr = stderr_stream_;
  }
  catch (...) {
    deconstruct();
    throw;
  }
}

Test::~Test() {
  deconstruct();

  bool success = false;

  [&] {
    Q_ASSERT(init_          | cleanup_          );
    QCOMPARE(init_          , cleanup_          );

    Q_ASSERT(init_test_case_| cleanup_test_case_);
    QCOMPARE(init_test_case_, cleanup_test_case_);

    success = true;
  }();

  Q_ASSERT(success);

  for (QString& logfile : logfiles_) {
    QString  bakfile = logfile + Logfile_Suffix;

    QFile lfile(logfile);
    lfile.open(QIODevice:: ReadOnly);

    success &= lfile.isOpen();

    QFile bfile(bakfile);
    bfile.open(QIODevice::WriteOnly | QIODevice::Append);

    success &= bfile.isOpen();

    while (!lfile.atEnd()) {
      QByteArray chunk = lfile.read(Logfile_Chunks);
      bfile.write(chunk);
    }

    lfile.close();
    success &= lfile.remove();

    bfile.close();
    success &= bfile.rename(logfile);
  }

  Q_ASSERT(success);
}

// ----------

void Test::init() {
  init_ ++;

  QVERIFY(qtemporarydir_ = new QTemporaryDir());
  QVERIFY(qtemporarydir_->isValid());

  current_path_ = QDir::currentPath();
  desired_path_ = qtemporarydir_->path();

  if (!desired_path_.isEmpty()) {
    QVERIFY(QDir::setCurrent(desired_path_));
  }
}

void Test::initTestCase() {
  init_test_case_ ++;

  qtMessageHandler = qInstallMessageHandler(&message_handler);
}

void Test::cleanup() {
  cleanup_ ++;

  if (!current_path_.isEmpty()) {
    QVERIFY(QDir::setCurrent(current_path_));
  }

  current_path_.clear();
  desired_path_.clear();

  if (qtemporarydir_) {
    delete qtemporarydir_;
    qtemporarydir_ = nullptr;
  }
}

void Test::cleanupTestCase() {
  cleanup_test_case_ ++;

  if (qtMessageHandler) {
    qInstallMessageHandler(qtMessageHandler);
    qtMessageHandler = nullptr;
  } else {
    qInstallMessageHandler(nullptr);
  }
}

// ----------

int Test::execute_impl(const QString    & program,
                       const QStringList& progargs) {
  execute_stdout_.clear();
  execute_stderr_.clear();

  QProcess qprocess;
  qprocess.start(program, progargs);

  LVD_LOG_D() << "execute"
              << qprocess.program()
              << qprocess.arguments();

  if (!qprocess.waitForStarted ())
    return -2;

  if (!qprocess.waitForFinished())
    return -2;

  if ( execute_stdout_ = qprocess.readAllStandardOutput();
      !execute_stdout_.isEmpty()) {
    if (execute_stdout_.endsWith("\n")) {
      execute_stdout_.chop(1);
    }

    lvd::qStdOut() << execute_stdout_ << Qt::endl;
  }

  if ( execute_stderr_ = qprocess.readAllStandardError ();
      !execute_stderr_.isEmpty()) {
    if (execute_stderr_.endsWith("\n")) {
      execute_stderr_.chop(1);
    }

    lvd::qStdErr() << execute_stderr_ << Qt::endl;
  }

  auto status = qprocess.exitStatus();
  if (status != QProcess::NormalExit) {
    return -1;
  }

  return qprocess.exitCode();
}

// ----------

void Test::deconstruct() {
  if (qtMessageHandler) {
    qInstallMessageHandler(qtMessageHandler);
    qtMessageHandler = nullptr;
  } else {
    qInstallMessageHandler(nullptr);
  }

  if (lvd::__impl__::qStdErrPtr == stderr_stream_) {
    lvd::__impl__::qStdErrPtr = nullptr;
  }

  delete stderr_stream_;
  delete stderr_device_;

  if (lvd::__impl__::qStdOutPtr == stdout_stream_) {
    lvd::__impl__::qStdOutPtr = nullptr;
  }

  delete stdout_stream_;
  delete stdout_device_;
}

// ----------

QStringList Test::logfiles_;
bool        Test::logfiles_gathered_ = false;

}  // namespace lvd::test
