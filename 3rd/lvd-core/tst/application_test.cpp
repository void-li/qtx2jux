/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "core.hpp"  // IWYU pragma: keep

#include <QMetaObject>
#include <QObject>
#include <QString>

#include "application.hpp"

#include "test.hpp"
using namespace lvd;
using namespace lvd::test;

// ----------

class ApplicationTest : public Test {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void success() {
    QMetaObject::invokeMethod(this, [&] {
      return;
    }, Qt::QueuedConnection);

    QSignalSpy spy(this, &QObject::destroyed);
    QVERIFY(spy.isValid());
    QVERIFY(spy.wait(0) || true);

    QVERIFY(!exception_);
  }

  void failure() {
    QMetaObject::invokeMethod(this, [&] {
      LVD_THROW_RUNTIME("Mera Luna");
    }, Qt::QueuedConnection);

    QSignalSpy spy(this, &QObject::destroyed);
    QVERIFY(spy.isValid());
    QVERIFY(spy.wait(0) || true);

    QVERIFY( exception_);
  }

  void print() {
    lvd::Application::print();
  }

  // ----------

 private slots:
  void init() {
    Test::init();

    exception_ = false;
  }

  void initTestCase() {
    Test::initTestCase();

    exception_ = false;

    connect(static_cast<Application*>(Application::instance()),
            &Application::failure,
            static_cast<Application*>(Application::instance()),
            [&] { exception_ = true; });
  }

  void cleanup() {
    LVD_FINALLY {
      Test::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      Test::cleanupTestCase();
    };
  }

 private:
  bool exception_ = false;
};

#define QCoreApplication Application

LVD_TEST_MAIN(ApplicationTest)
#include "application_test.moc"  // IWYU pragma: keep
