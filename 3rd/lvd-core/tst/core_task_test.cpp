/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "core.hpp"  // IWYU pragma: keep

#include <QObject>
#include <QString>

#include "core_task.hpp"
#include "core_task.hxx"

#include "test.hpp"
using namespace lvd;
using namespace lvd::test;

// ----------

class CoreTaskTest : public Test,
                     public Task<CoreTaskTest> {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void initial() {
    QVERIFY( empty_states());
    QCOMPARE(size_states(), 0);
  }

  void tesh_push_state() {
    push_state(&CoreTaskTest::callT);
    QCOMPARE(head_state(), &CoreTaskTest::callT);

    QVERIFY(!empty_states());
    QCOMPARE(size_states(), 1);

    push_state(&CoreTaskTest::callF);
    QCOMPARE(head_state(), &CoreTaskTest::callF);

    QVERIFY(!empty_states());
    QCOMPARE(size_states(), 2);
  }

  // ----------

  void test_execute_t() {
    push_state(&CoreTaskTest::callT);

    QVERIFY(!empty_states());
    QCOMPARE(size_states(), 1);

    exec_state();

    QVERIFY( empty_states());
    QCOMPARE(size_states(), 0);
  }

  void test_execute_tf() {
    push_state(&CoreTaskTest::callT);
    push_state(&CoreTaskTest::callF);

    QVERIFY(!empty_states());
    QCOMPARE(size_states(), 2);

    exec_state();

    QVERIFY(!empty_states());
    QCOMPARE(size_states(), 1);
  }

  void test_execute_f() {
    push_state(&CoreTaskTest::callF);

    QVERIFY(!empty_states());
    QCOMPARE(size_states(), 1);

    exec_state();

    QVERIFY( empty_states());
    QCOMPARE(size_states(), 0);
  }

  void test_execute_ft() {
    push_state(&CoreTaskTest::callF);
    push_state(&CoreTaskTest::callT);

    QVERIFY(!empty_states());
    QCOMPARE(size_states(), 2);

    exec_state();

    QVERIFY( empty_states());
    QCOMPARE(size_states(), 0);
  }

  // ----------

  void test_pull_state() {
    push_state(&CoreTaskTest::callT);
    QCOMPARE(head_state(), &CoreTaskTest::callT);

    push_state(&CoreTaskTest::callF);
    QCOMPARE(head_state(), &CoreTaskTest::callF);

    State state = pull_state();
    QCOMPARE(     state  , &CoreTaskTest::callF);

    QCOMPARE(head_state(), &CoreTaskTest::callT);
  }

  void test_clear_states() {
    push_state(&CoreTaskTest::callT);
    QCOMPARE(head_state(), &CoreTaskTest::callT);

    push_state(&CoreTaskTest::callF);
    QCOMPARE(head_state(), &CoreTaskTest::callF);

    QVERIFY(!empty_states());
    QCOMPARE(size_states(), 2);

    clear_states();

    QVERIFY( empty_states());
    QCOMPARE(size_states(), 0);
  }

  // ----------

  void test_drop_state_negative() {
    push_state(&CoreTaskTest::callT);
    push_state(&CoreTaskTest::callF);
    push_state(&CoreTaskTest::callX);

    drop_state(&CoreTaskTest::callT);

    QCOMPARE(head_state(), &CoreTaskTest::callX);
    QCOMPARE(size_states(), 3);
  }

  void test_drop_state_positive() {
    push_state(&CoreTaskTest::callT);
    push_state(&CoreTaskTest::callF);
    push_state(&CoreTaskTest::callX);

    drop_state(&CoreTaskTest::callF, &CoreTaskTest::callX);

    QCOMPARE(head_state(), &CoreTaskTest::callF);
    QCOMPARE(size_states(), 2);
  }

  void test_drop_state_positive2() {
    push_state(&CoreTaskTest::callT);
    push_state(&CoreTaskTest::callF);
    push_state(&CoreTaskTest::callX);

    drop_state(&CoreTaskTest::callX, &CoreTaskTest::callF);

    QCOMPARE(head_state(), &CoreTaskTest::callF);
    QCOMPARE(size_states(), 2);
  }

  void test_drop_state_positive3() {
    push_state(&CoreTaskTest::callT);
    push_state(&CoreTaskTest::callF);
    push_state(&CoreTaskTest::callX);

    drop_state();

    QCOMPARE(head_state(), &CoreTaskTest::callF);
    QCOMPARE(size_states(), 2);
  }

  // ----------

  void test_drop_states_negative() {
    push_state(&CoreTaskTest::callT);
    push_state(&CoreTaskTest::callF);
    push_state(&CoreTaskTest::callX);

    drop_states(&CoreTaskTest::callT);

    QCOMPARE(head_state(), &CoreTaskTest::callX);
    QCOMPARE(size_states(), 3);
  }

  void test_drop_states_positive() {
    push_state(&CoreTaskTest::callT);
    push_state(&CoreTaskTest::callF);
    push_state(&CoreTaskTest::callX);

    drop_states(&CoreTaskTest::callF, &CoreTaskTest::callX);

    QCOMPARE(head_state(), &CoreTaskTest::callT);
    QCOMPARE(size_states(), 1);
  }

  void test_drop_states_positive2() {
    push_state(&CoreTaskTest::callT);
    push_state(&CoreTaskTest::callF);
    push_state(&CoreTaskTest::callX);

    drop_states(&CoreTaskTest::callX, &CoreTaskTest::callF);

    QCOMPARE(head_state(), &CoreTaskTest::callT);
    QCOMPARE(size_states(), 1);
  }

  // ----------

 private:
  bool callT() { return true ; }
  bool callF() { return false; }
  bool callX() { return true ; }

  // ----------

 private slots:
  void init() {
    Test::init();

    clear_states();
  }

  void initTestCase() {
    Test::initTestCase();

    clear_states();
  }

  void cleanup() {
    LVD_FINALLY {
      Test::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      Test::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(CoreTaskTest)
#include "core_task_test.moc"  // IWYU pragma: keep
